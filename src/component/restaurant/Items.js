import React,{useState, useEffect} from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import Avatar from "@material-ui/core/Avatar";
import swal from "sweetalert";
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import Button from "@material-ui/core/Button";
import Snackbar from "@material-ui/core/Snackbar";
import { isEmpty, isAlphabets } from "../Checks";
import reactHTML from "react-render-html";
import CloseIcon from "@material-ui/icons/Close";
import { getData, postData, postDataAndImage } from "../../FetchNodeServices";

const useStyles = makeStyles((theme) => ({
  root:{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      marginTop: 20,
      padding: 10,
    },
    subdiv: {
      width: 610,
      background: "#ffeaa7",
      padding: 10,
    },
    input: {
      display: "none",
    },
    formControl: {
      minWidth: 300,
    },
    formControlfooditems: {
      minWidth: 290,
    },
}));

export default function Items(props) {
const classes = useStyles();
const [open, setOpen] = React.useState(false);
const [getFoodTypes, setFoodTypes] = useState([]);
const [getItems, setItems] = useState([]);
const [FoodImage, setFoodImage] = useState({
  bytes: "",
  file: "/noimage.jpg",
});
const [FoodtypeadImage, setFoodtypeadImage] = useState({
  bytes: "",
  file: "/noimage.jpg",
}); 
//console.log("PROPS:",props)
const [restaurant_id, setRestaurantid] = useState(props.restaurant.restaurantid);
const [foodtype, setFoodType] = useState("");
const [status, setstatus] = useState("");
const [fooditem, setFoodItem] = useState("");
const [price, setPrice] = useState("");
const [offer, setOffer] = useState("");
const [offertype, setOfferType] = useState("");
const [foodingredients, setFoodIngredients] = useState("");
const [rating, setRating] = useState("");
const [errorMessage, setErrorMessage] = useState("");
//const [foodTypeId, setFoodTypeId] = useState("");

const fetchFoodTypes = async () => {
  var body={restaurant_id:props.restaurant.restaurantid}
  var list = await postData("foodTypes/listfoodtypesbyrestaurant",body);
  setFoodTypes(list);
};

  const fillFoodTypes = () => {
  return getFoodTypes.map((item, index) => {
    return <MenuItem value={item.foodtypes_id}>{item.foodtype}</MenuItem>;
  });
}; 

const handleFoodTypesChange = async (event) => {
  setFoodType(event.target.value);
};

const fillFoodItems = () => {
  return getItems.map((item, index) => {
    return <MenuItem value={item.item_id}>{item.fooditem}</MenuItem>;
  });
};

useEffect(function () {
  fetchFoodTypes();
}, []);


const handleSubmit = async () =>  
{
    var msg = "";
    var err = false;
    if (isEmpty(restaurant_id)) {
      err = true;
      msg += "<b>Restaurant Id Should Not Be Empty...<b><br>";
    }
     if (isEmpty(status)) {
      err = true;
      msg += "<b>Status Should Not Be Empty.</b><br>";
    }
    if (err) {
      setErrorMessage(msg);
      setOpen(true);
    }
    if(!err)
    {
      var formData = new FormData();
      formData.append("restaurantid",restaurant_id );
      formData.append("foodtype", foodtype);
      formData.append("fooditem",fooditem)
      formData.append("FoodImage", FoodImage.bytes);
      formData.append("price", price);
      formData.append("offer", offer);
      formData.append("offertype",offertype);
      formData.append("foodingredients",foodingredients);
      formData.append("rating",rating);
      formData.append("foodtypeadimage", FoodtypeadImage.bytes);
      formData.append("status", status);
      var config = { headers: { "content-type": "multipart/form-data" } };
      var res = await postDataAndImage(
        "pPus/addnewitems",
        formData,
        config
      );
      //alert(res.result)
      if (res.result) 
      {
        swal
        ({
          title: "New Restaurant Added Successfully",
          icon: "success",
          dangerMode: true,
        });
      }
      else 
      {
        swal
        ({
          title: "Add New Restaurant?",
          text: "Fail to Add New Restaurant",
          icon: "warning",
          dangerMode: true,
        });
      }
    }  
}

const handleClose = () => {
  setOpen(false);
};
  return (
    <div className={classes.root}>
      <div className={classes.subdiv}>
        
        <Grid container spacing={1}>
          <Grid item xs={12}>
             <TextField
                label="Restaurant Id"
                value={restaurant_id}
                disabled={true}
                fullWidth
                onChange={(event) => setRestaurantid(event.target.value)}
                variant="outlined"
             />
           </Grid>
           <Grid item xs={12} sm={6}>
           <FormControl
              variant="outlined"
              className={classes.formControlfooditems}
            >
              <InputLabel>Food Type</InputLabel>
              <Select
                //value={Type}
                onChange={(event) => handleFoodTypesChange(event)}
                label=" Food Type"
              >
             {fillFoodTypes()} 
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              label="Food Item"
              fullWidth
              variant="outlined"
              onChange={(event) => setFoodItem(event.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField
              label="Price"
              fullWidth
              variant="outlined"
              onChange={(event) => setPrice(event.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField
              label="Offer"
              fullWidth
              variant="outlined"
              onChange={(event) => setOffer(event.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField
              label="Offer Type"
              fullWidth
              variant="outlined"
              onChange={(event) => setOfferType(event.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormControl variant="outlined" className={classes.formControl}>
              <InputLabel>Type</InputLabel>
              <Select
                //value={age}
                onChange={(event) => setstatus(event.target.value)}
                label="Status"
                fullWidth
              >
                <MenuItem value={"Veg"}>Veg</MenuItem>
                <MenuItem value={"Non-Veg"}>Non-Veg</MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              label="Ratings"
              fullWidth
              variant="outlined"
              onChange={(event) => setRating(event.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <TextField
              label="Food Ingredients"
              fullWidth
              variant="outlined"
              onChange={(event) => setFoodIngredients(event.target.value)}
            />
          </Grid>
          <Grid item xs={12} sm={6}>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <input
                accept="image/*"
                className={classes.input}
                id="icon-button-fssai"
                type="file"
                multiple
                 onChange={(event) =>
                  setFoodImage({
                    bytes: event.target.files[0],
                    file: URL.createObjectURL(event.target.files[0]),
                  })
                } 
              />
              <InputLabel><b> Food Image </b></InputLabel>
              <label htmlFor="icon-button-fssai" >
                <IconButton
                  color="primary"
                  aria-label="upload picture"
                  component="span"
                >
                  <PhotoCamera />
                </IconButton>
              </label>
              <Avatar
                alt="Remy Sharp"
                variant="rounded"
                style={{ marginLeft: 20 }}
                src={FoodImage.file}
                className={classes.large}
              />
            </div>
          </Grid>
           <Grid item xs={12} sm={6}>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <input
                accept="image/*"
                className={classes.input}
                id="icon-button-act"
                type="file"
                multiple
                onChange={(event) =>
                  setFoodtypeadImage({
                    bytes: event.target.files[0],
                    file: URL.createObjectURL(event.target.files[0]),
                  })
                }
              />
              <InputLabel><b> Food Type Add </b></InputLabel>
              <label htmlFor="icon-button-act">
                <IconButton
                  color="primary"
                  aria-label="upload picture"
                  component="span"
                >
                  <PhotoCamera />
                </IconButton>
              </label>
              <Avatar
                alt="Remy Sharp"
                variant="rounded"
                style={{ marginLeft: 20 }}
                src={FoodtypeadImage.file}
                className={classes.large}
              />
            </div>
          </Grid>
           
          <Grid item xs={12} sm={6}>
            <Button
              onClick={() => handleSubmit()}
              variant="contained"
              fullWidth
              color="primary"
            >
              Submit
            </Button>
          </Grid>

          <Grid item xs={12} sm={6}>
            <Button variant="contained" fullWidth color="primary">
              Reset
            </Button>
          </Grid>

        </Grid>  
    </div>
    <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
        message={reactHTML(errorMessage)}
        action={
          <React.Fragment>
            <Button color="secondary" size="small" onClick={handleClose}>
              UNDO
            </Button>
            <IconButton
              size="small"
              aria-label="close"
              color="inherit"
              onClick={handleClose}
            >
              <CloseIcon fontSize="small" />
            </IconButton>
          </React.Fragment>
        }
      />
    </div>
  );
}
