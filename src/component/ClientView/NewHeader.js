import React from "react";
import { fade, makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import InputBase from "@material-ui/core/InputBase";
import Badge from "@material-ui/core/Badge";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MailIcon from "@material-ui/icons/Mail";
import { useSelector } from "react-redux";
import ShoppingCart from "@material-ui/icons/ShoppingCart";
import MoreIcon from "@material-ui/icons/MoreVert";
import clsx from "clsx";
import Paper from "@material-ui/core/Paper";
import Drawer from "@material-ui/core/Drawer";
import Grid from "@material-ui/core/Grid";
import { ServerURL } from "../../FetchNodeServices";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import { Button, Divider } from "@material-ui/core";
import { NavLink, Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({}));

export default function NewHeader(props) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  var restaurant = useSelector((state) => state.restaurant);
  //alert(JSON.stringify(restaurant));
  var rest = Object.values(restaurant)[0];
  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  var cart = useSelector((state) => state.cart);
  var keys = Object.keys(cart);
  var values = Object.values(cart);
  var totalamt = values.reduce(calculate, 0);
  var totalsaving = values.reduce(totaloffer, 0);

  function calculate(prev, item) {
    var price =
      item.offer == 0
        ? item.qtydemand * item.price
        : item.qtydemand * item.offer;
    return prev + price;
  }

  function totaloffer(prev, item) {
    var price =
      item.offer > 0 ? (item.price - item.offer) * item.qtydemand : item.offer;
    return prev + price;
  }

  var netamount = totalamt - totalsaving;
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const handleProceed = () => {
    props.history.push({ pathname: "/signin" });
  };

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      <MenuItem onClick={handleMenuClose}>My account</MenuItem>
    </Menu>
  );

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === "top" || anchor === "bottom",
      })}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <Paper
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          padding: 15,
          fontSize: 16,
          fontWeight: 300,
          letterSpacing: 1,
          marginBottom: 5,
        }}
      >
        Food Selected({keys.length})
      </Paper>
      <Grid container spacing={1} style={{ marginBottom: 5 }}>
        {showFoodCart()}
      </Grid>

      <Paper
        style={{
          background: "#f5f6fa",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          padding: 10,
          fontSize: 16,
          fontWeight: 600,
          letterSpacing: 1,
          marginBottom: 5,
        }}
      >
        Payment Details
      </Paper>
      <div style={{ padding: 5 }}>
        <Grid container spacing={1}>
          <Grid item xs={6} style={{ padding: 3 }}>
            Total Amount:
          </Grid>
          <Grid item xs={6} style={{ textAlign: "right" }}>
            &#8377; {totalamt}
          </Grid>

          <Grid item xs={6} style={{ padding: 3 }}>
            Total Saving:
          </Grid>
          <Grid item xs={6} style={{ textAlign: "right" }}>
            &#8377; {totalsaving}
          </Grid>

          <Grid item xs={6} style={{ padding: 3 }}>
            Delivery Charges:
          </Grid>
          <Grid item xs={6} style={{ textAlign: "right" }}>
            &#8377; {0}
          </Grid>

          <Grid item xs={12}>
            <Divider />
          </Grid>
          <Grid item xs={6} style={{ padding: 3 }}>
            Net Amount:
          </Grid>
          <Grid item xs={6} style={{ textAlign: "right" }}>
            &#8377; {netamount}
          </Grid>
        </Grid>
      </div>
      <div style={{ padding: 3, marginTop: 10 }}>
        <Button
          fullWidth
          variant="contained"
          onClick={() => handleProceed()}
          color="primary"
        >
          Proceed
        </Button>
      </div>
    </div>
  );

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton aria-label="show 4 new mails" color="inherit">
          <Badge badgeContent={4} color="secondary">
            <MailIcon />
          </Badge>
        </IconButton>
        <p>Messages</p>
      </MenuItem>
      <MenuItem>
        <IconButton
          onClick={toggleDrawer("right", true)}
          aria-label="show 11 new notifications"
          color="inherit"
        >
          <Badge badgeContent={keys.length} color="secondary">
            <ShoppingCart />
          </Badge>
        </IconButton>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  );

  const showFoodCart = () => {
    return values.map((items) => {
      return (
        <>
          <Grid item xs={3}>
            <img
              src={`${ServerURL}/images/${items.foodimage}`}
              style={{ borderRadius: 5 }}
              width="60"
            />
          </Grid>
          <Grid
            item
            xs={6}
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-around",
            }}
          >
            <div style={{ fontWeight: 600 }}>{items.fooditem}</div>
            <div>
              {items.offer > 0 ? (
                <span>
                  <s>&#8377; {items.price}</s> {items.offertype} &#8377;{" "}
                  {items.offer} X {items.qtydemand}
                </span>
              ) : (
                <span>
                  &#8377; {items.price} X {items.qtydemand}
                </span>
              )}
            </div>
          </Grid>
          <Grid
            item
            xs={3}
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "flex-end",
            }}
          >
            <div>
              <DeleteOutline />
            </div>

            {items.offer == 0 ? (
              <div>&#8377; {items.price * items.qtydemand}</div>
            ) : (
              <div>&#8377; {items.offer * items.qtydemand}</div>
            )}
          </Grid>
          <Grid items xs={12} style={{ padding: 3 }}>
            <Divider />
          </Grid>
        </>
      );
    });
  };

  return (
    <div>
      <header className="header_section" style={{ background: "black" }}>
        <div className="container">
          <nav className="navbar navbar-expand-lg custom_nav-container ">
            <a className="navbar-brand" href="#home">
              <span>Restaurant</span>
            </a>

            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className=""> </span>
            </button>

            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav  mx-auto ">
                <li className="nav-item active">
                  {/* <a className="nav-link" href="">
                    Home <span className="sr-only">(current)</span>
                  </a> */}
                  <Link to="#newhome" className="nav-link">
                    Home<span className="sr-only">(current)</span>
                  </Link>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#menu">
                    Menu
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#booktable">
                    Table
                  </a>
                </li>
                {/* <li className="nav-item">
                  <Link to="#booktable" className="nav-link">
                    Table
                  </Link>
                </li> */}
                {/* <li className="nav-item">
                  <a className="nav-link" href="">
                    Book Table
                  </a>
                </li> */}
              </ul>
              <div className="user_option">
                <div className="user_link">
                  <i className="fa fa-user" aria-hidden="true"></i>
                </div>
                {/* <a href="" className="user_link">
                  <i className="fa fa-shopping-cart" aria-hidden="true"><Badge badgeContent={keys.length} color="secondary">
            <ShoppingCart />
          </Badge></i>
                </a> */}
                <MenuItem>
                  <IconButton onClick={toggleDrawer("right", true)}>
                    <Badge
                      badgeContent={keys.length}
                      style={{ color: "white" }}
                    >
                      <ShoppingCart />
                    </Badge>
                  </IconButton>
                </MenuItem>
                <form className="form-inline">
                  <button
                    className="btn  my-2 my-sm-0 nav_search-btn"
                    type="submit"
                  >
                    <i className="fa fa-search" aria-hidden="true"></i>
                  </button>
                </form>
                {/*  <a href="" className="order_online">
                  Order Online
                </a> */}
              </div>
            </div>
          </nav>
        </div>
      </header>
      <div className={classes.grow}>
        {/* <AppBar position="static">
        <Toolbar> */}
        {/*  <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="open drawer"
          >
            <MenuIcon />
          </IconButton> */}
        {/* {JSON.stringify(restaurant)!='{}' ? (
            <div className={classes.title}>
              
            </div>
          ) : (
            <></>
          )} */}
        {/* <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ "aria-label": "search" }}
            />
          </div> */}
        {/* <div className={classes.grow} />
          <div className={classes.sectionDesktop}> */}
        {/* <IconButton aria-label="show 4 new mails" color="inherit">
              <Badge badgeContent={4} color="secondary">
                <MailIcon />
              </Badge>
            </IconButton> */}
        {/* <IconButton
              onClick={toggleDrawer("right", true)}
              aria-label="show 17 new notifications"
              color="inherit"
            >
              <Badge badgeContent={keys.length} color="secondary">
                <ShoppingCart />
              </Badge>
            </IconButton> */}
        {/*  <IconButton
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <AccountCircle />
            </IconButton> */}
        {/*  </div> */}
        {/* <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div> */}
        {/*  </Toolbar>
      </AppBar> */}
        {/*  {renderMobileMenu}
      {renderMenu} */}

        <div>
          <React.Fragment key={"right"}>
            <Drawer
              anchor={"right"}
              open={state["right"]}
              onClose={toggleDrawer("right", false)}
            >
              {list("right")}
            </Drawer>
          </React.Fragment>
        </div>
      </div>
    </div>
  );
}
