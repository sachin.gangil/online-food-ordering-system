import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Header from "./Header";
import {useHistory} from "react-router-dom"
import {useDispatch,useSelector} from "react-redux"
import Footer from './Footer'

const useStyles = makeStyles({
  root: {
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
},
subdiv:{width: 550,
  padding: 20,
  marginTop: 20,
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
},
card: {
  border:'1px solid #bdc3c7',
},
cardactionarea: {
  borderBottom: '1px solid #bdc3c7',
  borderTop: '2px solid #bdc3c7'
},
cardmedia: {
  borderBottom: '1px solid #bdc3c7'
},
});
function Page1(props) {
  var client = useSelector((state) => state.client);
  var user = Object.values(client)[0];
  var history=useHistory()
  var dispatch=useDispatch()
  const handleHomeDelivery=()=>{
    client[user.mobileno]['deliveryat']="Home Delivery"
    console.log("Client",client)
    console.log("user",user)
    history.push({'pathname':'/page2'})

    

  }

  const handleTakeAway=()=>{
    client[user.mobileno]['deliveryat']="Take Away"
    console.log("Client",client)
    console.log("user",user)
    dispatch({type:'ADD_CLIENT', payload:[user.mobileno,user]})
    history.push({'pathname':'/page2'})

    

  }

  
  
  const classes = useStyles();
  return (
    <div>
      {/* <Header history={props.history} /> */}
      <header className="header_section" style={{ background: "black" }}>
        <div className="container">
          <nav className="navbar navbar-expand-lg custom_nav-container ">
            <div className="navbar-brand">
              <span>Uncle Restaurant</span>
            </div>
            {/* <a className="navbar-brand" href="#">
              <span>Restaurant</span>
            </a> */}

            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className=""> </span>
            </button>

            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <div className="user_option">
                <div className="user_link">
                  {/* <MenuItem onClick={handleProfileMenuOpen}>
                    <IconButton
                      aria-label="account of current user"
                      aria-controls="primary-search-account-menu"
                      aria-haspopup="true"
                      color="inherit"
                    >
                      <AccountCircle />
                    </IconButton>
                  </MenuItem> */}
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>
    
    <div className={classes.root}>
      
      <div className={classes.subdiv}>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <Card className={classes.card}>
              <CardActionArea className={classes.cardactionarea}>
                <CardMedia className={classes.cardmedia} style={{height:200}}
                  component="img"
                  alt="Home Delivery"
                  image="/home delivery1.jpg"
                  title="Home Delivery"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    Home Delivery
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
                    across all continents except Antarctica
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button variant="contained" color="primary" fullWidth onClick={()=>handleHomeDelivery()}>Home Delivery</Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Card className={classes.card}>
              <CardActionArea className={classes.cardactionarea}>
                <CardMedia className={classes.cardmedia} style={{height:200}}
                  component="img"
                  alt="Take Away"
                  image="/take away.jpg"
                  title="Take Away"
                  
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    Take Away
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
                    across all continents except Antarctica
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button variant="contained" color="primary" fullWidth onClick={()=>handleTakeAway()}>Take Away</Button>
              </CardActions>
            </Card>
          </Grid>
        </Grid>
      </div>
    </div>
    <Footer />
    </div>
  );
}

export default Page1;
