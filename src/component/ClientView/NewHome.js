import NewHeader from "./NewHeader";
import Footer from "./Footer";
import React, { useState, useEffect } from "react";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Header from "./Header";
import { ServerURL, getData, setData, postData } from "../../FetchNodeServices";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import QtySpinner from "./QtySpinner";
import { useDispatch, useSelector } from "react-redux";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { Affix } from "rsuite";
import BookTable from './Booktable';

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  root: {
    width: "100%",
    maxWidth: "36ch",
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: "inline",
  },

  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "flex",
    },
  },
  sectionMobile: {
    display: "flex",

    [theme.breakpoints.up("sm")]: {
      display: "none",
    },
    roott: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: "center",
      color: theme.palette.text.secondary,
    },
  },
}));
export default function NewHome(props) {
  const classes = useStyles();
  const [FoodTypes, setFoodTypes] = useState([]);
  const [FoodItemsList, setFoodItemsList] = useState([]);
  const [restaurant, setRestaurant] = useState([]);
  const [refresh, setRefresh] = useState(false);
  var dispatch = useDispatch();
  useEffect(function () {
    fetchFoodTypes(2);
    fetchByID(2);
    fetchFoodItemsByOffer(2);
  }, []);

  const fetchFoodTypes = async (resid) => {
    var body = { restaurant_id: resid };
    var list = await postData("foodTypes/listfoodtypesbyrestaurant", body);
    setFoodTypes(list);
  };

  const fetchByID = async (resid) => {
    var body = { restaurantid: resid };
    var list = await postData("restaurant/restaurantbyid", body);
    //  alert(JSON.stringify(list))
    list = list[0];
    dispatch({ type: "ADD_RES", payload: [list.restaurantid, list] });
    setRestaurant(list[0]);
  };

  const fetchFoodItemsByOffer = async (resid) => {
    var body = { restaurant_id: resid };
    var list = await postData("pPus/listfooditemsoffer", body);
    setFoodItemsList(list);
  };

  const fetchFoodItems = async (typeid) => {
    var body = { foodtype: typeid };
    var list = await postData("pPus/listfooditemsbyfoodtype", body);
    setFoodItemsList(list);
  };
  const handleChange = (value, item) => {
    if (value == 0) {
      dispatch({ type: "REMOVE_ITEM", payload: item.item_id });
      setRefresh(!refresh);
    } else {
      item["qtydemand"] = value;
      dispatch({ type: "ADD_ITEM", payload: [item.item_id, item] });
      setRefresh(!refresh);
    }
  };

  const displayFoodItems = () => {
    return FoodItemsList.map((item, index) => {
      return (
        <div
          style={{
            border: "0.5px solid #dfe6e9",
            borderRadius: 5,
            display: "flex",
            flexDirection: "column",
            margin: 10,
          }}
        >
          <div style={{ marginBottom: 5 }}>
            {
              (item.status = "Veg" ? (
                <img
                  src="/veg.png"
                  width="10"
                  style={{ position: "absolute", zIndex: 1, padding: 10 }}
                />
              ) : (
                <img
                  src="/nonveg.png"
                  width="10"
                  style={{ position: "absolute", zIndex: 1, padding: 10 }}
                />
              ))
            }
            <img
              src={`${ServerURL}/images/${item.foodimage}`}
              width="165"
              height="120"
              style={{ borderRadius: 5 }}
            />
            {/* <img src="chole.jfif" width="165" height='120'/> */}
          </div>
          <div
            style={{
              padding: 5,
              fontSize: 16,
              fontWeight: 800,
              letterSpacing: 1,
            }}
          >
            {item.fooditem}
            {/* {"Chola Bhatura"} */}
          </div>
          <div
            style={{
              fontSize: 12,
              padding: 5,
              fontWeight: 600,
              letterSpacing: 1,
              whiteSpace: "nowrap",
              width: "140px",
              overflow: "hidden",
              textOverflow: "ellipsis",
            }}
          >
            {item.foodingredients}
            {/*  {"chana Masala,Raita....."} */}
          </div>
          <div
            style={{
              fontSize: 10,
              padding: 5,
              fontWeight: 600,
              letterSpacing: 1,
            }}
          >
            {item.offer == 0 ? (
              <div style={{ display: "flex", flexDirection: "column" }}>
                <div style={{ fontWeight: 600 }}>MRP &#8377; {item.price}</div>

                <div style={{ fontWeight: 800, color: "green" }}>&nbsp;</div>
              </div>
            ) : (
              <div style={{ display: "flex", flexDirection: "column" }}>
                <div style={{ fontWeight: 600 }}>
                  MRP <s>&#8377; {item.price}</s> {item.offertype} &#8377;{" "}
                  {item.offer}
                </div>
                <div style={{ fontWeight: 800, color: "green" }}>
                  You Save &#8377; {item.price - item.offer}
                </div>
              </div>
            )}
          </div>
          <div
            style={{
              fontSize: 12,
              padding: 5,
              fontWeight: 600,
              letterSpacing: 1,
            }}
          >
            <span>{item.rating}/5</span> <img src="star.jpg" width="15" />
          </div>
          <div
            style={{
              width: 150,
              justifyContent: "center",
              alignItems: "center",
              padding: 8,
              display: "flex",
            }}
          >
            <QtySpinner
              value={0}
              onChange={(value) => handleChange(value, item)}
            />
          </div>
        </div>
      );
    });
  };

  const showFoodtypeDesktop = () => {
    return FoodTypes.map((item, index) => {
      return (
        <List className={classes.root}>
          <ListItem alignItems="flex-start">
            <ListItemAvatar>
              <Avatar
                alt="Remy Sharp"
                src={`${ServerURL}/images/${item.foodimage}`}
              />
            </ListItemAvatar>
            <ListItemText
              primary={item.foodtype}
              secondary={""}
              onClick={() => fetchFoodItems(item.foodtype)}
            />
          </ListItem>
          <Divider variant="inset" component="li" />
        </List>
      );
    });
  };

  const showFoodtypeMobile = () => {
    return FoodTypes.map((item, index) => {
      return (
        <div
          onClick={() => fetchFoodItems(item.foodtype)}
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            width: 75,
          }}
        >
          <Avatar
            alt="Remy Sharp"
            src={`${ServerURL}/images/${item.foodimage}`}
          />
          <div style={{ fontSize: 7, fontWeight: 800, letterSpacing: 1 }}>
            {item.foodtype}
          </div>
        </div>
      );
    });
  };

  return (
    <div>
      <div className="hero_area" id="home" >
        <div className="bg-box">
          <img src="images/hero-bg.jpg" alt="" />
        </div>
        <Affix top={0}>
          <NewHeader history={props.history} />
        </Affix>
        <section className="slider_section ">
          <div
            id="customCarousel1"
            className="carousel slide"
            data-ride="carousel"
          >
            <div className="carousel-inner">
              <div className="carousel-item active">
                <div className="container ">
                  <div className="row">
                    <div className="col-md-7 col-lg-6 ">
                      <div className="detail-box">
                        <h1>Fast Food Restaurant</h1>
                        <p>
                          Doloremque, itaque aperiam facilis rerum, commodi,
                          temporibus sapiente ad mollitia laborum quam quisquam
                          esse error unde. Tempora ex doloremque, labore, sunt
                          repellat dolore, iste magni quos nihil ducimus libero
                          ipsam.
                        </p>
                        <div className="btn-box">
                          <a href="" className="btn1">
                            Order Now
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="carousel-item ">
                <div className="container ">
                  <div className="row">
                    <div className="col-md-7 col-lg-6 ">
                      <div className="detail-box">
                        <h1>Fast Food Restaurant</h1>
                        <p>
                          Doloremque, itaque aperiam facilis rerum, commodi,
                          temporibus sapiente ad mollitia laborum quam quisquam
                          esse error unde. Tempora ex doloremque, labore, sunt
                          repellat dolore, iste magni quos nihil ducimus libero
                          ipsam.
                        </p>
                        <div className="btn-box">
                          <a href="" className="btn1">
                            Order Now
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="carousel-item">
                <div className="container ">
                  <div className="row">
                    <div className="col-md-7 col-lg-6 ">
                      <div className="detail-box">
                        <h1>Fast Food Restaurant</h1>
                        <p>
                          Doloremque, itaque aperiam facilis rerum, commodi,
                          temporibus sapiente ad mollitia laborum quam quisquam
                          esse error unde. Tempora ex doloremque, labore, sunt
                          repellat dolore, iste magni quos nihil ducimus libero
                          ipsam.
                        </p>
                        <div className="btn-box">
                          <a href="" className="btn1">
                            Order Now
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="container">
              <ol className="carousel-indicators">
                <li
                  data-target="#customCarousel1"
                  data-slide-to="0"
                  className="active"
                ></li>
                <li data-target="#customCarousel1" data-slide-to="1"></li>
                <li data-target="#customCarousel1" data-slide-to="2"></li>
              </ol>
            </div>
          </div>
        </section>
      </div>
      <section  id="menu">
        {/* <div className="container"> */}
        <div
          className="heading_container heading_center"
          style={{paddingBottom: "5px", paddingTop: "5px" }}
        >
          <h2>Our Menu</h2>
        </div>
        <div className="">
          <div className={classes.sectionDesktop}>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                marginTop: 10,
                width: "100%",
              }}
            >
              <Grid container spacing={1}>
                <Grid
                  item
                  xs="12"
                  style={{
                    textAlign: "-webkit-center",
                    display: "inline-flex",
                    paddingLeft: "25px",
                    justifyContent: "center",
                  }}
                >
                  {showFoodtypeDesktop()}
                </Grid>
                <Grid
                  item
                  xs="12"
                  style={{ display: "flex", justifyContent: "center" }}
                >
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      flexWrap: "wrap",
                      justifyContent: "center",
                      width: "60%",
                    }}
                  >
                    {displayFoodItems()}
                  </div>
                </Grid>
              </Grid>
            </div>
          </div>
          <div className={classes.sectionMobile}>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                marginTop: 10,
                width: "100%",
              }}
            >
              <Grid container spacing={1}>
                <Grid item xs="12" sm={6}>
                  <div style={{ display: "flex", flexDirection: "row" }}>
                    {showFoodtypeMobile()}
                  </div>
                </Grid>
                <Grid item xs="12" sm={6}>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      flexWrap: "wrap",
                    }}
                  >
                    {displayFoodItems()}
                  </div>
                </Grid>
              </Grid>
            </div>
          </div>
        </div>
        {/*  </div> */}
      </section>
       <section id="booktable">
        <BookTable />
      </section>
      <Footer />
    </div>
  );
}
