import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import { getData, postData, postDataAndImage } from "../../FetchNodeServices";
import { isEmpty, isAlphabets } from "../Checks";
import Snackbar from "@material-ui/core/Snackbar";
import CloseIcon from "@material-ui/icons/Close";
import renderHTML from "react-render-html";
import IconButton from "@material-ui/core/IconButton";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Footer from "./Footer";
import { NavLink, Link } from 'react-router-dom';
import { Email } from "@material-ui/icons";

export default function Booktable(props) {
  const [open, setOpen] = React.useState(false);
  const [name, setName] = useState("");
  const [number, setNumber] = useState("");
  const [email, setEmail] = useState("");
  const [personqty, setPersonqty] = useState("");
  const [date, setDate] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const handleSubmit = async () => {

    var msg = "";
    var err = false;
    if (isEmpty(name)) {
      err = true;
      msg += "<b> Name Should Not Be Empty...<b><br>";
    }

    if (isEmpty(number)) {
      err = true;
      msg += "<b>Number Should Not Be Empty...<b><br>";
    }
    if (isEmpty(email)) {
      err = true;
      msg += "<b>enter your email<b><br>";
    }

    /* if (!isEmpty(email)) {
      err = true;
      msg += "<b>Enter your email...<b><br>";
    } */

    if (err) {
      setErrorMessage(msg);
      setOpen(true);
    }
    if (!err){
    console.log(email);
    console.log(name);
    console.log(number);
    console.log(personqty);
    var formData = new FormData();
    formData.append("name", name);
    formData.append("number", number);
    formData.append("personqty", personqty);
    formData.append("email", email);
    formData.append("date", date);

    const formObj = {
      name:name,
    number:number,
    email:email,
    personqty:personqty,
    date:date


    }
   

    var config = { headers: { "content-type": "multipart/form-data" } };
    console.log('form data', formObj);
    var res = await postData("booktable/insertdatabooktable", formObj, config);
    if (res.result) {
      swal({
        title: "Your Booking Success",
        icon: "success",
        dangerMode: true,
      });
      clearData();
    } else {
      swal({
        title: "Table Booking",
        text: "Fail your Booking",
        icon: "warning",
        dangerMode: true,
      });
    }
  }
  };

  const handleClose = () => {
    setOpen(false);
  };

  const clearData = () => {
    setName("");
    setDate("");
    setNumber("");
    setEmail("");
    setPersonqty("");
  }

  return (
    <div>
      <section
        className="book_section layout_padding"
        style={{ marginLeft: "40px", marginRight: "40px" }}
      >
        <div>
          <div className="heading_container">
            <h2>Book A Table</h2>
          </div>

          <div className="row">
            <div className="col-md-6">
              <div className="form_container">
                <div>
                  <input
                    type="text"
                    className="form-control"
                    name="name"
                    value={name}
                    placeholder="Your Name"
                    onChange={(event) => setName(event.target.value)}
                  />
                </div>
                <div>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Phone Number"
                    name="number"
                    value={number}
                    onChange={(event) => setNumber(event.target.value)}
                  />
                </div>
                <div>
                  <input
                    type="email"
                    className="form-control"
                    placeholder="Your Email"
                    name="email"
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                  />
                </div>
                <div>
                  <select
                    className="form-control nice-select wide"
                    onChange={(event) => setPersonqty(event.target.value)}
                    value={personqty}
                    name="personqty"
                  >
                    <option value="" disabled selected>
                      How many persons?
                    </option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </select>
                </div>
                <div>
                  <input
                    type="date"
                    className="form-control"
                    onChange={(event) => setDate(event.target.value)}
                    value={date}
                    name="dob"
                  />
                </div>
                
                <div className="btn_box">
                  <button onClick={() => handleSubmit()}>Book Now</button>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              {/* <div className="map_container ">
                <div id="googleMap"></div>
              </div> */}
              <div className="container">
                <div className="row">
                  <div className="col-sm">
                    <div className="map-iframe">
                      <iframe
                        //  frameborder="1|0"
                        width="1340"
                        title="map"
                        height="541.25"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d317718.69319292053!2d-0.3817765050863085!3d51.528307984912544!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a00baf21de75%3A0x52963a5addd52a99!2sLondon%2C+UK!5e0!3m2!1sen!2spl!4v1562654563739!5m2!1sen!2spl"
                        style={{
                          border: 0,
                          //width: '100% ',
                          height: 350,
                          width: 400,
                        }}
                      ></iframe>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
        message={renderHTML(errorMessage)}
        action={
          <React.Fragment>
            <IconButton
              size="small"
              aria-label="close"
              color="inherit"
              onClick={handleClose}
            >
              <CloseIcon fontSize="small" />
            </IconButton>
          </React.Fragment>
        }
      />
    </div>
  );
}
