var express = require('express');
var router = express.Router();
var pool=require('./pool')


router.post("/insertdatabooktable", function (req, res, next) {
  console.log(req.body)
    pool.query(
      "insert into booktable(name,number,email,personqty,date)values(?,?,?,?,?)",
      [
        req.body.name,
        req.body.number,
        req.body.email,
        req.body.personqty,
        req.body.date,
      ],
      function (error, result) {
        if (error) {
          console.log(error);
          return res.status(500).json({ result: false });
        } else {
          console.log(result);
          return res.status(200).json({ result: true });
        }
      }
    );
  });

  module.exports = router;