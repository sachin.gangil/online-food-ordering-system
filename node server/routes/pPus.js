var express = require("express");
var router = express.Router();
var pool = require("./pool");
var upload = require("./multer");

router.post("/addnewitems", upload.any(), function (req, res, next) {
  pool.query("insert into item(restaurant_id,fooditem,foodtype,price,offer,offertype,rating,foodimage,foodtypead,status,foodingredients)values(?,?,?,?,?,?,?,?,?,?,?)",
    [
      req.body.restaurantid,
      req.body.fooditem,
      req.body.foodtype,
      req.body.price,
      req.body.offer,
      req.body.offertype,
      req.body.rating,
      req.files[0].originalname,
      req.files[1].originalname,
      req.body.status,
      req.body.foodingredients,
    ],
    function (error, result) {
      if (error) {
        console.log(error)
        res.status(500).json({ result: false });
      } else {
        res.status(200).json({ result: true });
      }
    }
  );
});

router.get("/fetchfooditems", function (req, res) {
 pool.query("select * from item",function(error,result){
 // pool.query("select R.*,(select S.foodtype from foodtypes S where S.foodtypes_id=R.foodtypes_id) as foodtypes from item R",function (error, result) {
      if (error) {
        console.log(error);
        res.status(500).json([]);
      } else {
        console.log(result);
        res.status(200).json(result);
      }
    }
  );
});

router.post("/editfooditems", function (req, res, next) {
  pool.query(
    "update item set foodtypes_id=?, fooditem=?, price=?, offer=?, offertype=?, fooditemtype=?, foodingredients=?, rating=? where restaurant_id=? and item_id=? ",
    [
      req.body.foodtypeid,
      req.body.fooditem,
      req.body.price,
      req.body.offer,
      req.body.offertype,
      req.body.foodtype,
      req.body.foodingredients,
      req.body.rating,
      req.body.restaurantid,
      req.body.fooditemid,
    ],
    function (err, result) {
      if (err) {
        console.log(req.body);
        res.status(500).json({ result: false });
      } else {
        res.status(200).json({ result: true });
      }
    }
  );
});

/* router.get("/listfooditems", function (req, res) {
  pool.query(
    "select F.*,(select ft.food_type from ftypes ft where ft.food_typeid=F.foodtype_id)as food_type,(select fi.food_itemname from fitems fi where fi.food_itemid=F.fooditem)as food_itemname from item F",
    function (err, result) {
      // pool.query("select * from fooditems",function(err,result){
      if (err) {
        res.status(500).json([]);
      } else {
        res.status(200).json(result);
      }
    }
  );
}); */

router.post(
  "/editfooditemimage",
  upload.single("foodimage"),
  function (req, res) {
    pool.query(
      "update item set foodimage=? where restaurant_id=? and item_id=?",
      [req.file.originalname, req.body.restaurantid, req.body.fooditem],
      function (err, result) {
        if (err) {
          res.status(500).json(false);
        } else {
          res.status(200).json(true);
        }
      }
    );
  }
);

router.post("/deletefooditem", function (req, res) {
  pool.query(
    "delete from item where restaurant_id=? and item_id=?",
    [req.body.restaurantid, req.body.item],
    function (err, result) {
      if (err) {
        console.log(req.body);
        res.status(500).json(false);
      } else {
        res.status(200).json(true);
      }
    }
  );
});

 router.post("/listfooditemsbyfoodtype", function (req, res) {
  pool.query(
    "select * from item where foodtype=?",[req.body.foodtype],
    function (err, result) {
      if (err) {
        res.status(500).json([]);
      } else {
        res.status(200).json(result);
      }
    }
  );
}); 

router.post("/listfooditemsoffer", function (req, res) {
  pool.query(
    "select * from item where restaurant_id=? and offer>0",[req.body.restaurant_id],
    function (err, result) {
    
      if (err) {
        console.log(err);
        res.status(500).json([]);
      } else {
        res.status(200).json(result);
      }
    }
  );
});


module.exports = router;