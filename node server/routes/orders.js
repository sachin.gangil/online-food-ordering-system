var express = require('express');
const pool = require('./pool');
var router = express.Router();

/* GET home page. */
router.post('/generateorder', function(req, res, next) {
  pool.query("insert into ordergeneration (orderdate,ordertime,mobileno,emailid,totalamount)values(?,?,?,?,?)",[req.body.orderdate,req.body.ordertime,req.body.mobileno,req.body.emailid,req.body.totalamt],function(error,result){
   if(error)
   {  console.log(error)
       res.status(500).json({result:false})}
  else
  {  
    
    res.status(200).json({result:true,orderid:result.insertId})
  }

  })
});

 router.post('/submitorder', function(req, res, next) {
    console.log(req.body)
    q ="insert into orders(orderid, orderdate, ordertime, mobileno, emailid,fooditem, qtydemand, price, offer,amount, deliverystatus, paymentstatus,paymentmode,restaurantid, deliveryat,item_id,totalamount) values ?";

    pool.query(
    q,
    [
      req.body.cart.map((item) => [
        req.body.orderid,
        req.body.orderdate,
        req.body.ordertime,
        req.body.mobileno,
        req.body.emailid,
        item.fooditem,
        item.qtydemand,
        item.price,
        item.offer,
        item.amount,
        req.body.deliverystatus,
        req.body.paymentstatus,
        req.body.paymentmode,
        req.body.restaurantid,
        req.body.deliveryat,
        item.item_id,
        req.body.totalamt
      ]),
    ],
    function (err, result) {
      if (err) {
        console.log(err);
        return res.status(500).json({ RESULT: false });
      } else {
        return res.status(200).json({ RESULT: true });
      }
    }
  );
  }); 
module.exports = router;
